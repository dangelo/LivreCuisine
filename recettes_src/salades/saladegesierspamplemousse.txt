# Salade de gésiers au pamplemousse

## Ingrédients
* 2 poignées de mesclun (salade mélangée
* 200 g de gésiers de volaille confits
* 1 pamplemousse rose
* 1 pamplemousse jaune
* 50 g de pignons de pin
* 2-3 pincées de piment doux
* 1 cuillère à soupe de vinaigre de cidre
* 3 cuillères à soupe d'huile
* Quelques brins de cerfeuil, sel, poivre

## Infos
* 4 personnes
* Préparation : 20 min

## Étapes
* Laver la salade. Peler à vif les 2 pamplemousses. Découper la chair en morceaux.
* Dans une poêle, faire dorer les pignons de pin dans une cuillère à soupe d'huile bien chaude et les réserver.
* Dans la même poêle, faites revenir les gésiers.
* Émulsionner deux cuillères à soupe d'huile et le vinaigre avec le piment doux.
* Mélanger l'ensemble des ingrédients délicatement en rectifiant l'assaisonnement si nécessaire. Parsemer de cerfeuil avant de servir.


