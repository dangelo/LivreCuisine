# Biscuits de Noël à la cannelle (Schwowebredele)

## Ingrédients
* 270 g de beurre
* 500 g de farine
* 2 œufs + 1 pour la dorure
* 150 g de poudre d'amandes
* 15 g de cannelle
* 1 zeste de citron râpé
* 250 g de sucre 

## Infos
* Pour 1 grande boîte ($\approx$ 120 pièces)
* Préparation : 45 min + 12~h de repos
* Cuisson : 10 min / fournée  ($\approx$ 40 min)

## Étapes
* Dans un grand saladier, mélanger le beurre ramolli avec la farine du bout des doigts pour obtenir une texture sablonneuse.
* Ajouter 2 œufs, la poudre d'amandes, la cannelle, le zeste de citron et le sucre et pétrir jusqu'à obtenir une pâte homogène. Ne pas hésiter à rajouter un peu de farine si la pâte est trop collante.
* Filmer et laisser reposer une nuit dans un endroit frais.
* Le lendemain, préchauffer le four à 200° C.
* Étaler la pâte (sur une épaisseur comprise entre 3 et 10~mm selon les goûts), découper les biscuits à l'aide d'emporte-pièces et les déposer sur une plaque de cuisson.
* Battre le troisième œuf avec une petite pincée de sel. Dorer les biscuits au pinceau et faire cuire 10 min environ (le temps peut varier selon le four, bien surveiller).
* À la sortie du four, attendre une ou deux minutes avant de déposer les biscuits sur une grille et les laisser refroidir complètement.



## Conseils
Il est possible d'en faire une version sans cannelle, parfumée seulement avec le zeste de citron.

Les biscuits peuvent se conserver plusieurs semaines dans une boîte en métal.

