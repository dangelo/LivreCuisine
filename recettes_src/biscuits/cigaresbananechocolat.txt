# Cigares banane-choco

## Ingrédients
* 3 feuilles de brick
* 2 bananes
* 12 carrés de chocolat noir
* 25 g de beurre

## Infos
* Pour 4 personnes*
* Préparation : 15 min
* Cuisson : 10 min

## Étapes
* Faites fondre le beurre au micro-onde à puissance moyenne. 
* Allumez le four à 210° C, th 7.
* Épluchez les bananes et coupez-les en trois tronçons, puis chacun en deux dans la longueur.
* Coupez les feuilles de brick en quatre triangles. 
* Beurrez-les au pinceau.
* Déposez la partie la plus large vers vous. Posez un tronçon de banane et un carré de chocolat dessus. Rabattez les côtés, enroulez. Formez ainsi 12 petits cigares.
Déposez-les sur une plaque recouverte de papier cuisson. 
* Cuisez 10 minu four.

## Conseils
Servez les cigares tout chauds avec une boule de glace à la vanille.

Vin : banyuls ou maury

