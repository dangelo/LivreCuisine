# Gâteau au chocolat (façon Claire T.)
\choco{Gâteau au chocolat (façon Claire T.)}
## Ingrédients
* 5 oeufs
* 150 g de sucre
* 250 g de chocolat
* 100 g d'amandes en poudre
* 200 g de beurre
* 3 c. à soupe de fécule de pomme de terre
* 2 sachets de sucre vanillé
* 1/2 c. à café de levure
* Pour le glaçage :

* 125 g de chocolat à croquer
* 50 g de beurre
* 100 g de sucre glace


## Infos
* Pour 6 personnes
* Préparation : 45 min
* Cuisson : 40 min

## Étapes
* Fouetter les jaunes d'oeufs avec les sucres jusqu'à ce qu'ils blanchissent et fassent ruban.
* Faire fondre le chocolat avec deux cuillères à soupe d'eau au bain-marie non bouillant. L'ajouter aux jaunes, mélanger.
* Ajouter la poudre d'amandes tamisée avec la levure et la fécule, le beurre très ramolli mais non fondu, puis les blancs d'oeufs battus en neige avec une pincée de sel.
* Verser dans un moule de 24 cm de diamètre, beurré et saupoudré de sucre semoule, en ne remplissant qu'aux trois quarts et mettre au four th 6-7. Démouler tiède.
* Pour le glaçage, faire fondre le chocolat avec une cuillère à soupe d'eau et le beurre au bain-marie. Ajouter le sucre glace tamisé par cuillères puis enduire le gâteau.


