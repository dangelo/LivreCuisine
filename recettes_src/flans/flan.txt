# Flan aux pruneaux

## Ingrédients
* 1 litre de lait
* 10 cuillère à soupe de maïzena
* 150 g de sucre
* vanille
* pruneaux (facultatif)

## Infos
* Pour 6 personnes
* Préparation : 15 min	

## Étapes
* Délayer la maïzena dans un peu de lait.
* Faire chauffer le reste du lait sucré.
* À ébullition verser la maïzena, et laisser cuire quelques secondes.
* Ajouter la vanille.
* Beurrer un plat, disposer les pruneaux dénoyautés et verser la crème.
* Mettre au four 30 min (thermostat 5)

