# Tarte mousseuse aux framboises

## Ingrédients
* Pour la pâte :

*	250 g de farine
*	10 g de levure de boulanger
*	1 oeuf
*	40 g de sucre
*	10 cl de lait
*	un quart de cuil. à café de sel
*	100 g de beurre ramolli

* Pour la garniture :

*	300 g de framboises \index{framboises}
*	100 g de sucre
*	3 oeufs
*	1 cuil. à soupe de sucre glace
*	125 g d’amandes en poudre


## Infos
* Pour 6 personnes
* Préparation : 45 min
* Repos : 1 h
* Cuisson : 40 min

## Étapes
*  Dans une terrine, délayez la levure dans 5 cl de lait tiède et une pincée de sucre. Ajoutez 60 g de farine et mélangez pour obtenir une sorte de bouillie. Couvrez d’un linge et laissez lever 15 minans un endroit tiède.
*  Versez le reste de farine sur le levain, ajoutez le reste de lait tiède, le sel, le sucre et l’oeuf. Pétrissez la pâte à la main ou au robot, jusqu’à ce qu’elle soit homogène. Incorporez le beurre et travaillez jusqu’à ce que la pâte se détache et ne colle plus aux mains (ou 5 minu robot).
* Laissez reposer sous un linge 45 minans un endroit tiède. La pâte doit doubler de volume.
*  Pétrissez la pâte 30 secondes pour la faire retomber. Beurrez un moule rectangulaire et garnissez-le avec la pâte, en appuyant pour faire adhérer.
*  Allumez le four th. 7 (210° C). Laissez la pâte au tiède pendant que vous préparez la garniture.
*  Séparez les blancs des jaunes d’oeufs. Fouettez les jaunes avec 50 g de sucre jusqu’à ce que le mélange blanchisse. Ajoutez la poudre d’amandes, mélangez. Battez les blancs en neige ferme avec le reste du sucre et incorporez délicatement cette mousse dans la masse précédente.
*  Étalez cette préparation sur le fond de tarte, puis répartissez les framboises à la surface. Elles vont s’enfoncer dans la crème au cours de la cuisson. Enfournez la tarte à mi-hauteur et faites cuire pendant environ 40 min
*  Laissez refroidir 5 à 10 minans le moule, pus démoulez la tarte sur une grille. Poudrez de sucre glace avant de déguster. 

