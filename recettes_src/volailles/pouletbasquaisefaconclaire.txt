# Poulet basquaise (façon Claire T.)

## Ingrédients
* 6 morceaux de poulet \index{poulet}
* 1 kg de tomates \index{tomate}
* 700 g de poivrons (verts et rouges) \index{poivron}
* 3 oignons émins \index{oignon}
* 3 gousses d'ail 
* 1 verre de vin blanc
* 1 bouquet garni, 
* huile d'olive, 
* poivre, sel

## Infos
* Pour 6 personnes
* Préparation : 35 min
* Cuisson : 55 min

## Étapes
* Dans une cocotte, faire dorer dans l'huile d'olive les morceaux de poulet salés et poivrés. Réserver.
* Faire chauffer 4 cuillères à soupe d'huile, y faire dorer les oignons, l'ail pressé, les poivrons taillés en lanières. Laisser cuire 5 min
* Laver, éplucher et couper les tomates en morceaux, les ajouter à la cocotte, sel, poivre. Couvrir et laisser mijoter 20 min
* Ajouter le poulet aux légumes, ajouter le bouquet garni et le vin blanc, couvrir et laisser cuire à feu très doux 35 min

## Conseils
Attention à la cuisson, ça accroche vite...


