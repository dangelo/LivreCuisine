# Rôti de porc braisé aux fenouils

## Ingrédients
* 1 rôti de porc d’environ 1kg
* 4 bulbes de fenouil
* 1 oignon
* 2 feuilles de laurier
* 15 cl de vin blanc sec
* 2 cuil. à soupe d’huile
* 1 cuil. à soupe d’herbes de Provence
* 1 cuil. à café de poivre mignonnettes, sel.

## Infos
* 4 personnes
* Prep. 10 min
* Cuisson 35 min

## Étapes
Nettoyez les bulbes de fenouil et coupez-les en quartiers. 
Dans un autocuiseur, faites dorer le rôti de porc avec l’huile chaude et réservez-le. A sa place, faites revenir rapidement les quartiers de fenouil et l’oignon émincé.
Remettez le rôti dans l’autocuiseur au centre des légumes et parsemez-le avec les herbes de Provence et le poivre mignonnette. Versez le vin blanc sur les fenouils, ajouter le laurier, salez.
Fermez l’autocuiseur et comptez 30 min de cuisson à partir de la rotation de la soupape. Placez le rôti dans un plat avec les légumes et servez vite.


