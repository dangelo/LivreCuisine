# Papillote de saumon et julienne de légumes au boursin

## Ingrédients
* 2 pavés de saumon
* 250 g de julienne de légumes surgelée
* boursin cuisine ail et fines herbes

## Infos
* Pour 2 personnes
* Préparation : 15 min
* Cuisson : 15 min

## Étapes
* Si le saumon n'est pas congelé, faire décongeler la juliene de légumes. 
* Préchauffer le four à 180° C.
* Découper 2 grandes feuilles de papier sulfurisé. Ôter la peau du saumon. 
* Répartir environ 1/4 de la julienne sur chaque feuille, déposer un pavé de saumon par dessus, puis étaler un peu de boursin dessus.
* Mélanger 2 à 3 cuillères à soupe de boursin avec le reste de julienne, puis la répartir sur et autour du saumon.
* Fermer les papillottes, et les faire cuire au four 15 min(30 si les produits sont congelés).


